#!/bin/bash
# автор — Михаил Новоселов <mikhailnov@dumalogiya.ru>

# avoid odd symbols in ls output if it's a presetup alias in the shell (such as in Rosa Fresh)

alias ls="$(which ls)"

CONFIG_DIR_GLOBAL="/etc/dumalogiya"
# Надо бы использовать что-то типа $XDG_CONFIG_HOME, но такого , судя по всему, из коробки нет
CONFIG_DIR_USER="$HOME/.config/dumacast"

# функция для вывода одной и той же ошибки одновременно и в консоль, и в Zenity; cz - console, zenity
function zenity_error {
	echo "$@" | logger
	zenity --error --width=450 --text="$@"
}
function zenity_info {
	echo "$@" | logger
	zenity --info --width=450 --text="$@"
}
function env_override {
	# эта функция позволяет переменными окружения с префиксом DUMACAST_ переопределить значения переменных в конфигурационных файлах
	# Например, можно указать DUMACAST_CHROMIUM="/usr/bin/google-chrome", чтобы переменная CHROMIUM приняла значение /usr/bin/google-chrome (CHROMIUM="/usr/bin/google-chrome"), несмотря на то, что указано во всех конфигах
	while read -r line
	do
		overriden_var_name="$(echo $line | awk -F "=" '{print $1}' | sed 's/^DUMACAST_//')"
		overriden_var_value="$(echo $line | awk -F "=" '{print $2}')"
		export "$overriden_var_name"="$overriden_var_value"
		echo "Overriden $overriden_var_name with $overriden_var_value as set by env variable" | logger
	done < <(env | grep ^DUMACAST_)
}

# check if we can write to ${tmp} or write to the home directory if ${tmp} is not writable; exit if none of the directories are writable
tmp="$HOME"
[ -w /tmp ] && tmp="/tmp"
[ -w "$tmp" ] || ( zenity_error "Ни /tmp/, ни домашняя директория $HOME недоступны для записи — Думакаст не может продолжить работать!"; exit 1 )

# проверим, есть ли уже папка в домашней директории с конфигами, если нет, создадим ее и скопируем туда конфиги из папки глобальных конфигов
if [ -d "$CONFIG_DIR_USER" ]
	then
		echo "Папка с конфигами внутри домашней папки пользователя найдена, предполагаем, что раз есть папка, то в ней есть и конфиги"
		# README.md was depreceated, remove the old and broken symlink
		rm -fv "$CONFIG_DIR_USER/README.md" || true
	else
		echo "Папка с конфигами внутри домашней папки пользователя НЕ найдена, создадим ее и скопируем туда конфиги"
		mkdir -p "$CONFIG_DIR_USER"
		# for i in REAL_MICROPHONE.example REAL_MICROPHONE REAL_OUTPUT.example REAL_OUTPUT WEBCAM.example WEBCAM 
		for i in dumacast-config.sh
		do
			cp -rv "$CONFIG_DIR_GLOBAL/$i" "$CONFIG_DIR_USER/$i"
		done
		#ln -s "$CONFIG_DIR_GLOBAL/README.md" "$CONFIG_DIR_USER/README.md"
fi

# теперь перенесем содержимое существующих конфигов в новые (пеерехжаем на новый формат конфигов). CONFIG_DIR_GLOBAL не задаем переменной, т.к. у старой версии нашей программы была фиксированная диркетория с конфигами
# получится, что внутри конфига сначала будет присваиваться одно (стандаратное или пустое) значение переменной, а следующими строками — перенесенное из старых конфигов
# если файл .block-v2-to-v3-config-merge существует, то значит, что перенос конфигов уже выполнен, и при этом запуске не нужно их переносить заново
if ! [ -f "$CONFIG_DIR_USER/.block-v2-to-v3-config-merge" ]; then
	for i in WEBCAM REAL_MICROPHONE REAL_OUTPUT
	do
		if ! [ -z "$(cat /etc/dumalogiya/$i)" ]; then
			echo "" >> "$CONFIG_DIR_USER/dumacast-config.sh"
			echo "$i=\"$(cat /etc/dumalogiya/$i)\" " >> "$CONFIG_DIR_USER/dumacast-config.sh"
		fi
	done
	touch "$CONFIG_DIR_USER/.block-v2-to-v3-config-merge"
fi

# check if the kernel module v4l2loopback is already loaded, otherwise load it
# dumacast-modprobe is a script which allows to perform "modprobe v4l2loopback" from userspace via a sudoers rule (SUID does not work on non-binary executables)
( ( lsmod | grep -Fq v4l2loopback ) || sudo dumacast-modprobe ) || zenity_error "Модуля ядра (драйвер) v4l2loopback не загружен, почти весь функционал Думакаста, кроме трансляции экрана в соцсети, не заработает, но мы продолжим работать"

VIRT_CAMERA="/dev/$(ls /sys/devices/virtual/video4linux/ | head -n1)"
# подгружаем настройки из файлов-конфигов
source "$CONFIG_DIR_GLOBAL/dumacast-config.sh"
source "$CONFIG_DIR_USER/dumacast-config.sh"
# таким образом, если в конфигах не задана виртуальная веб-камера, то будет использоваться первая в системе, а если задана, то параметр будет перезаписан
# dumacast-config-overrides.sh используется для переназначения каких-либо настроек путем обновления пакета в репозиториях, сделан на всякий случай, как резервный механизм внесения изменений в настройки поверх пользовательских
source "$CONFIG_DIR_GLOBAL/dumacast-config-overrides.sh"
env_override

if ps aux | grep -Fqi "$(which pulseaudio)"
	then
		echo "Pulseaudio найдена, продолжаем работать..."
	else
		zenity_error "Звуковая система Pulseaudio не запущена, без нее не сможем сделать виртуальный микрофон, почти весь функционал Думакаста не будет работать!"
fi
if which ffmpeg >/dev/null
	then
		echo "FFmpeg найден, продолжаем работать..."
	else
		zenity_error "FFmpeg не найден, не можем продолжить работать! Если вы на Ubuntu 14.04/Debian 8/Mint 17, то установите FFmpeg из дополнительных репозиториев! На libav скрипт не проверяли, возможно, будет работать."
		exit 1
fi
# check if ffmpeg supports needed codecs and encoders/decoders
for check_codec in aac libx264
do
	( ffmpeg -codecs 2>/dev/null | grep -Fq "$check_codec" ) || zenity_error "FFmpeg не поддерживает кодек/кодер/декодер ${check_codec}. Думакаст будет работать неправильно!"
done
# check if ffmpeg supports pulseaudio output
if [[ -z $(ffmpeg -sources pulse 2>/dev/null) ]]
	then
		zenity_error "Ваш FFmpeg $(which ffmpeg) не поддерживает вывод аудио в PulseAudio, Dumacast может работать частично некорректно!"
fi
# check if FFmpeg supports the h264_nvenc codec for Nvidia-accelerated encoding
if ffmpeg -codecs 2>/dev/null | grep -Fq "h264_nvenc"
	then
		FFMPEG_SUPPORTS_NVENC_H264=1
		# FFMPEG_USE_NVENC can be overriden by configs or DUMACAST_FFMPEG_USE_NVENC environmental variable (see the function env_override)
		### раскоментировать строку ниже, как скрипт научится определять, поддерживает ли имеющаяся карта NVENC на самом деле
		#FFMPEG_USE_NVENC=1
	else
		FFMPEG_SUPPORTS_NVENC_H264=0
fi

# проверим наличие браузера, с которым мы должны работать
echo "Predefined Chromium: $CHROMIUM"
# head -n 1 защищает от сулчая, если CHROMIUM= несколько значений через пробел, тогда which тоже выдаст несколкьо ответов
if ! [ -f "$(which $CHROMIUM | head -n 1)" ]; then
	if which chromium-browser
		then
			CHROMIUM="$(which chromium-browser)"
		else
			if which chromium; then CHROMIUM="$(which chromium)"; else
			echo "Chromium не найден, попробуем найти другие браузеры на движке Chromium"
			# если и Хром не найдется, то чуть дальше по коду будет exit 1
			# ищем браузеры на движке Chromium в PATH; вывод сортируется по алфавиту; google-chrome окажется первым по алфавиту; https://unix.stackexchange.com/a/50613
			CHROMIUM="$(find ${PATH//:/ } -maxdepth 1 -name "google-chrome*" -o -name "chromium*" -o -name "qupzilla*" -o -name "falkon*" -o -name "opera*" -o -name "yandex-browser*" -o -name "vivaldi*" | head -n1)"
			fi
	fi
fi
if [ -f "$(which $CHROMIUM | head -n 1)" ];
		then
		   echo "Браузер на движке Chromium найден, запустим его."
		else
		   zenity_error "Ошибка! Браузер на движке Chromium не найден, через Firefox виртуальный микрофон работать не будет! Не можем продолжить работать!"
		   exit 1
fi
	

# проверим, чтобы виртуальная камера и настоящая камера не были одним и тем же устройством
if [[ "$WEBCAM" == "$VIRT_CAMERA" ]]; then
	zenity_error "Виртуальная и настоящая веб-камеры — одно и то же устройство! Это ошибка в настройках, Думакаст будет работать неправильно!"
fi

RESOLUTION="${IMG_WIDTH}x${IMG_HEIGHT}"
echo "Разрешение картинки: $RESOLUTION"

echo "Webcamera: $WEBCAM"

echo "Виртуальная камера: $VIRT_CAMERA"
if [ -c "$VIRT_CAMERA" ];
	then
	   echo "Виртуальная веб-камера $VIRT_CAMERA найдена, продолжаем работать..."
	else
	   zenity_error "Не найдена виртуальная веб-камера, почти весь функционал Думакаста не будет работать! Загрузите модуль ядра v4l2loopback (команда: sudo modprobe v4l2loopback), предварительно подключив настоящую веб-камеру, если она не подключена, после этого заново запустите Думакаст!"
fi
if [[ "$CHROMIUM_RESTART" == "yes" ]]
	then
		killall "$CHROMIUM"
		pkill "$CHROMIUM"
fi
killall "$(which pavucontrol)"

if [ "$PULSEAUDIO_RESTART" == "yes" ]
	then
		pulseaudio --kill
		sleep 5
		pulseaudio --start
		sleep 10
fi
echo "Real microphone: $REAL_MICROPHONE"
echo "Real output: $REAL_OUTPUT"
# эта такой баг, наверное, v4l2loopback: если пустить в виртуальную веб-камеру картинку с камеры после локального видео или наоборот при условии, что соотношение их ширины и всоты разное, то на незанытой новым разрешением области будет мигать последний кадр предыдущей трансляции, поэтому на несколько секунд пускаем полностью черное видео, чтобы избежать такого эффекта
EMPTY_VIDEO="$CONFIG_DIR_GLOBAL/empty.mp4"
if [ -f "$EMPTY_VIDEO" ]
	then
		echo "Видео-заглушка найдена"
	else
		zenity_error "Не найдена черная видео-заглушка, изображение в виртуальной веб-камере может передаваться некорректно!"
fi

# проверим, настроены эти нужные нам параметры
for i in REAL_MICROPHONE REAL_OUTPUT WEBCAM VIRT_CAMERA CHROMIUM
do
	# check if the string (variable) is null
	if [ -z "$i" ]
		then
			zenity_error "Не найдена настройка ${i}, без нее почти весь функционал Думакаста не будет работать!"
	fi
done

pactl load-module module-null-sink sink_name=ffmpegplay sink_properties=device.description="Проигрывание_видеоролика"
pactl load-module module-null-sink sink_name=null1 sink_properties=device.description="null1"
pactl load-module module-loopback source="$REAL_MICROPHONE" sink=null1
pactl load-module module-loopback source=ffmpegplay.monitor sink="$REAL_OUTPUT"
pactl load-module module-loopback source=ffmpegplay.monitor sink=null1
pactl set-sink-volume null1 100%
pactl set-sink-volume ffmpegplay 100%
pactl set-source-volume null1.monitor 100%
pactl set-source-volume ffmpegplay.monitor 100%

### не могу вспомнить, зачем это нужно делать, скорее всего, это ничего не дает, позже нужно проверить
v4l2-ctl -d "$VIRT_CAMERA" -c timeout=1500

function kill_ffmpeg_all { 
	echo "kill_ffmpeg_all begin..."
	for kill_target in $(cat ${tmp}/ffmpeg-*-dumacast.pid)
	do
		kill "$kill_target"
		# run kill -9 for the case if they have not been killed by kill and suppress its errors by 2>/dev/null
		kill -9 "$kill_target" 2>/dev/null
	done 
	echo "Killing -9 lsof -t $WEBCAM"
	# check if WEBCAM variable is not null and only than kill, without this check lsof -t returns a lot of userspace PIDs and the session stops after killing them
	if [ -n "$WEBCAM" ]
		then
			kill $(lsof -t $WEBCAM)
			kill -9 $(lsof -t $WEBCAM) 2>/dev/null
	fi
}
function ffmpeg_empty_video {
	#ffmpeg -i "$EMPTY_VIDEO" -f v4l2 -pix_fmt rgb24 -vf scale=w=${IMG_WIDTH}:h=${IMG_HEIGHT}:force_original_aspect_ratio=decrease "$VIRT_CAMERA" & echo $! >${tmp}/ffmpeg-nullframe-dumacast.pid
	ffmpeg -t 3 -f lavfi -i color=c=black -pix_fmt rgb24 -vf scale=w=${IMG_WIDTH}:h=${IMG_HEIGHT}:force_original_aspect_ratio=decrease -f v4l2 "$VIRT_CAMERA" & \
	echo $! >${tmp}/ffmpeg-nullframe-dumacast.pid
}
function ffmpeg_real_webcam {
	ffmpeg -f video4linux2 -i "$WEBCAM" -f v4l2 -pix_fmt rgb24 -vf scale=w=${IMG_WIDTH}:h=${IMG_HEIGHT}:force_original_aspect_ratio=decrease "$VIRT_CAMERA" &
	echo $! >${tmp}/ffmpeg-webcam-dumacast.pid
}
function keep_format {
	v4l2-ctl -d "$VIRT_CAMERA" -c keep_format=${1}
}

kill_ffmpeg_all
echo "dumacast: 1) VIRT_CAMERA: $VIRT_CAMERA; webcam: $WEBCAM; REAL_OUTPUT: $REAL_OUTPUT" | logger
echo "dumacast: 2) VIRT_CAMERA: $VIRT_CAMERA; webcam: $WEBCAM; REAL_OUTPUT: $REAL_OUTPUT" | logger
ffmpeg_empty_video
ffmpeg_real_webcam &

function ffmpeg_video2webcam {
	if [ "$1" = 'loop' ]; then
		# -loop -1 means continuously repeating the video
		ffmpeg_video2webcam_loop='-loop -1'
	fi
	filename="$(zenity --file-selection --title="Выбор видео/аудио для проигрывания")"
	case $? in
		0)
			kill_ffmpeg_all
			keep_format 1
			ffmpeg_empty_video
			keep_format 0
			ffmpeg_empty_video
			keep_format 1
			# won't the shell be forked with all variables lost? probably won't
			#-pix_fmt rgb24 -vf scale=w=${IMG_WIDTH}:h=${IMG_HEIGHT}:force_original_aspect_ratio=decrease
			( env PULSE_SINK=ffmpegplay ffmpeg ${ffmpeg_video2webcam_loop} -re -i "$filename" -pix_fmt rgb24 -vf scale=w=${IMG_WIDTH}:h=${IMG_HEIGHT}:force_original_aspect_ratio=decrease -f v4l2 "$VIRT_CAMERA" -f pulse default & echo $! >${tmp}/ffmpeg-video-dumacast.pid ) && ffmpeg_real_webcam &
		;;
		*)
			zenity_info "Не выбран файл, продолжаем работать без проигрывания видео или аудио в виртуальные камеру и микрофон"
		;;
	esac
}
function choose_action {
	CHOICE=$(zenity --list --radiolist --height=450 \
		   --width="800" \
		   --title="Выбор дальнейшего действия" \
		   --text="Что будем делать?" \
		   --column="да/нет" --column="Что будем делать?" \
		   TRUE "Пустить в эфир видео с веб-камеры" \
		   FALSE "Трансляция видео/аудио без повтора в вирт. веб-камеру" \
		   FALSE "Трансляция видео/аудио с бесконечным повтором в вирт. веб-камеру" \
		   FALSE "Использовать камеру Android вместо веб-камеры (приложение IP Webcam)" \
		   FALSE "Запустить трансляцию экрана в соцсети" \
		   FALSE "Остановить трансляции экрана в соцсети" \
		   FALSE "Не завершая трансляции проиграть АУДИО в эфир" \
		   FALSE "Пустить в эфир черную заглушку" \
		   FALSE "Завершить все трансляции" \
		   FALSE "Полностью выйти из программы трансляции" )
		   
	case $CHOICE in 
		"Пустить в эфир видео с веб-камеры")
			kill_ffmpeg_all
			keep_format 1
			ffmpeg_empty_video
			ffmpeg_real_webcam & 
		;;
		"Трансляция видео/аудио без повтора в вирт. веб-камеру")
			ffmpeg_video2webcam noloop
		;;
		"Трансляция видео/аудио с бесконечным повтором в вирт. веб-камеру")
			ffmpeg_video2webcam loop
		;;
		"Не завершая трансляции проиграть АУДИО в эфир")
			filename="$(zenity --file-selection --title="Выбор аудио для проигрывания")"
			case $? in
				0)
					env PULSE_SINK=ffmpegplay ffplay "$filename" & echo $! >${tmp}/ffmpeg-audio-dumacast.pid &
				;;
				*)
					zenity_info "Не выбран файл, продолжаем работать без проигрывания аудио в виртуальные камеру и микрофон"
				;;
			esac 
		;;
		"Завершить все трансляции")
			kill_ffmpeg_all
			zenity_info "Все трансляции в виртуальную веб-камеру завершены!"
		;;
		"Полностью выйти из программы трансляции")	
			kill_ffmpeg_all
			keep_format 1
			rm -fv ${tmp}/ffmpeg-*-dumacast.pid
			exit 0
		;;
		"Пустить в эфир черную заглушку")
			kill_ffmpeg_all
			ffmpeg_empty_video
		;;
		"Использовать камеру Android вместо веб-камеры (приложение IP Webcam)")
			ffmpeg_empty_video #на всякий случай очистим весь кадр камеры
			kill_ffmpeg_all
			keep_format 0
			CAMERA_IP_ADRESS=$(zenity --forms --title="Параметры IP-камеры" --text="Пример: 192.168.1.166:8080 \nТелефон и компьютер должны быть подключены к одной и той же сети (например,WiFi)" --add-entry="IP-адрес и порт камеры через двоеточие")
			# now test the camera
			if ffmpeg -i "http://${CAMERA_IP_ADRESS}/videofeed" -t 00:00:01 -f null /dev/null
				then
					echo "Проверка возможности получения изображения с камеры пройдена успешно."
					# откроем в браузере дополнительную вкладку с веб-интерфейсом управления камерой телефона (Android-приложение IP Webcam)
					OPEN_URL="http://${CAMERA_IP_ADRESS}"
					PULSE_SOURCE=null1.monitor "$CHROMIUM" "$OPEN_URL" &
					# do not scale the picture scaling if we get the image from an IP camera (?)
					ffmpeg -i "http://${CAMERA_IP_ADRESS}/videofeed" -f v4l2 -pix_fmt rgb24 "$VIRT_CAMERA" &
					echo $! >${tmp}/ffmpeg-ipcam-dumacast.pid
				else 
					zenity_error "Ошибка! Проверка возможности получения изображения с камеры НЕ пройдена: с IP камеры не удалось получить изображение, попробуйте настроить ее заново."
			fi
			
		;;
		"Запустить трансляцию экрана в соцсети")
			# обнулим переменные на случай, чтобы, если трансляция запускается повторно, не сохранились их предыдущие значения, даже если одна из этих трансляций сейчас не запускается
			FFMPEG_RTMP_VK_CLI=""
			FFMPEG_RTMP_FB_CLI=""
			# FFMPEG_USE_NVENC должна быть 1, только если пройдена проверка, что видеокарта действительно справляется с кодированием таким кодеком!
			### можно попробовать делать одно кодирование (например, во Вконтакте) на GPU, а второе (Facebook) на CPU
			if [[ "$FFMPEG_USE_NVENC" == 1 ]] && [[ "$FFMPEG_SUPPORTS_NVENC_H264" == 1 ]]
				then
					FFMPEG_H264_ENCODER="h264_nvenc"
				else
					FFMPEG_H264_ENCODER="libx264"
			fi
			# https://www.ibm.com/developerworks/ru/library/l-zenity/
			function zenity_choose_where_broadcast {
				zenity --list --checklist --multiple \
				--text="Выберите, в какие соцсети вести трансляцию экрана" \
				--column="Пометить" --column="Соцсеть" \
				--print-column="2" --separator="\n" \
				TRUE "Вконтакте" \
				TRUE "Facebook"
				#FALSE "Одноклассники"
			}
			zenity_info "После нажатия на ОК нужно выбрать область экрана, которую транслировать в соцсети. Выбранная область помечается желтым прямоугольником."
			read -r X Y W H G ID < <(slop --noopengl --bordersize=5 --color='189,1,0' -f "%x %y %w %h %g %i")
			for broadcast_target in $(zenity_choose_where_broadcast)
			do
				case "$broadcast_target" in
					"Вконтакте" )
						while true
						do
							RTMP_VK_URL=$(zenity --forms --title="Настройки трансляции во Вконтакте" --text="Вконтакте при создании трансляции в разделе \"Настройки видеокодера\" показал URL и KEY. Введите URL." --add-entry="URL")
							RTMP_VK_KEY=$(zenity --forms --title="Настройки трансляции во Вконтакте" --text="Вконтакте при создании трансляции в разделе \"Настройки видеокодера\" показал URL и KEY. Введите KEY." --add-entry="KEY")
							if [ ! -z "$RTMP_VK_URL" ] && [ ! -z "$RTMP_VK_KEY" ]
								then
									### еще нужно проверять URL на валидность (начинается с rtmp:// и пр.)
									break
								else
									zenity_error "Вы не ввели либо URL, либо KEY, попробуйте еще раз!"
							fi
						done
						# crf 28, veryfast применяются для снижения нагрузки на процессор машины, с которой производится трансляция
						FFMPEG_RTMP_VK_CLI="-f flv -c:v $FFMPEG_H264_ENCODER -crf 28 -c:a aac -preset veryfast ${RTMP_VK_URL}/${RTMP_VK_KEY}"
					;;
					"Facebook" )
						while true
						do
							RTMP_FB_URL=$(zenity --forms --title="Настройки трансляции в Facebook" --text="В Facebook при создании трансляции перейдите во вкладку \"Подключиться\" вместо \"Камера\". Там указаны URL-адрес сервера и ключ потока. Введите URL-АДРЕС СЕРВЕРА." --add-entry="URL")
							RTMP_FB_KEY=$(zenity --forms --title="Настройки трансляции во Facebook" --text="В Facebook при создании трансляции перейдите во вкладку \"Подключиться\" вместо \"Камера\". Там указаны URL-адрес сервера и ключ потока. Введите КЛЮЧ ПОТОКА." --add-entry="KEY")
							if [ ! -z "$RTMP_FB_URL" ] && [ ! -z "$RTMP_FB_KEY" ]
								then
									### еще нужно проверять URL на валидность (начинается с rtmp:// и пр.)
									break
								else
									zenity_error "Вы не ввели либо URL-адрес сервера, либо ключ потока, попробуйте еще раз!"
							fi
						done
						FFMPEG_RTMP_FB_CLI="-f flv -c:v $FFMPEG_H264_ENCODER -crf 28 -c:a aac -preset veryfast ${RTMP_FB_URL}/${RTMP_FB_KEY}"
					;;
					
				esac
			done
			# теперь запустим трансляцию во все соцсети сразу
			PULSE_SOURCE=null1.monitor ffmpeg -f x11grab -s "$W"x"$H" -r 15 -i ${DISPLAY}+$X,$Y -f pulse -i default ${FFMPEG_RTMP_VK_CLI} ${FFMPEG_RTMP_FB_CLI}
			echo $! >${tmp}/ffmpeg-rtmp_all-dumacast0.pid #dumacast0, а не dumacast, чтобы этот процесс не убивался функцией kill_ffmpeg_all
		;;
		"Остановить трансляции экрана в соцсети")
			kill "$(cat ${tmp}/ffmpeg-rtmp_all-dumacast0.pid)"
			kill -9 "$(cat ${tmp}/ffmpeg-rtmp_all-dumacast0.pid)"
			zenity_info "Все трансляции экрана в соцсети были остановлены."
			keep_format 1
		;;
	esac
}

while true; do choose_action; done & \
pavucontrol & PULSE_SOURCE=null1.monitor "$CHROMIUM" "$OPEN_URL"
ffmpeg_empty_video
